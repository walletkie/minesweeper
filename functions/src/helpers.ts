import { Response } from 'firebase-functions';

export const Helpers = {
    setHeaders(response: Response): void {
        response.set('Access-Control-Allow-Origin', '*');
        response.set('Access-Control-Allow-Headers', 'authorization, content-type, origin, accept');
        response.set('Access-Control-Allow-Credentials', 'true');
        response.set("Content-Type", "application/json");
    }
}