import * as functions from 'firebase-functions';
import { Helpers } from './helpers';

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

export const helloWorld = functions.https.onRequest((request, response) => {
    Helpers.setHeaders(response);
    
    const data = {
        data: 'hello wornld!'
    }
    response.send(data);
});

export const getServerTime = functions.https.onRequest((request, response) => {
    Helpers.setHeaders(response);
    
    const data = {
        data: Date.now()
    }
    response.send(data);
});
