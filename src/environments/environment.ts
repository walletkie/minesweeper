// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBNhlH5ena8vmU8mX1NY38KX5r_pIz96bk',
    authDomain: 'minesweeper-78aff.firebaseapp.com',
    databaseURL: 'https://minesweeper-78aff.firebaseio.com',
    projectId: 'minesweeper-78aff',
    storageBucket: 'minesweeper-78aff.appspot.com',
    messagingSenderId: '579292005464',
    appId: '1:579292005464:web:b12ba01e469d7396f0b62d',
    measurementId: 'G-C4VD754L87'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
