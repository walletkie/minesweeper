import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MinesweeperComponent } from './components/minesweeper/minesweeper.component';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatCardModule,
  MatMenuModule,
  MatIconModule,
  MatTooltipModule,
  MatTableModule,
  MatPaginatorModule,
  MatSlideToggleModule,
  MatPaginatorIntl,
  MatSelectModule,
  MatDividerModule,
  MatSnackBarModule
} from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faFlag,
  faBomb,
  faFireAlt,
  faCog,
  faShareAlt,
  faUsers,
  faUserLock,
  faUserAlt,
  faUserAltSlash,
  faInfoCircle,
  faSignInAlt,
  faLock,
  faLockOpen
} from '@fortawesome/free-solid-svg-icons';
import {
  faFlag as faFlag2,
  faSmile,
  faClock,
  faCopy
} from '@fortawesome/free-regular-svg-icons';
import { MinesweeperBoardComponent } from './components/minesweeper-board/minesweeper-board.component';
import { LongpressDirective } from './driectives/longpress.directive';
import { MinesweeperFieldComponent } from './components/minesweeper-field/minesweeper-field.component';
import {
  MinesweeperIngameOptionsComponent
} from './components/minesweeper-ingame-options/minesweeper-ingame-options.component';
import { FormsModule } from '@angular/forms';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { TestComponent } from './components/test/test.component';
import { LoginComponent } from './components/login/login.component';
import { GenerateRoomComponent } from './components/generate-room/generate-room.component';
import { RoomsListComponent } from './components/rooms-list/rooms-list.component';
import { ShareRoomComponent } from './components/share-room/share-room.component';
import { PlayersListComponent } from './components/players-list/players-list.component';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { AuthenticationService } from './services/authentication.service';
import { getPaginatorIntl } from './components/rooms-list/paginator-localization';
import { MinesweeperGlobalOptionsComponent } from './components/minesweeper-global-options/minesweeper-global-options.component';
import { CookieService } from 'ngx-cookie-service';
import { ChangeLanguageComponent } from './components/change-language/change-language.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    MinesweeperComponent,
    MinesweeperBoardComponent,
    LongpressDirective,
    MinesweeperFieldComponent,
    MinesweeperIngameOptionsComponent,
    TestComponent,
    LoginComponent,
    GenerateRoomComponent,
    RoomsListComponent,
    ShareRoomComponent,
    PlayersListComponent,
    MinesweeperGlobalOptionsComponent,
    ChangeLanguageComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    FontAwesomeModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatRadioModule,
    MatTooltipModule,
    MatListModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    AngularFireFunctionsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatDividerModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MatSnackBarModule
  ],
  exports: [
    MinesweeperIngameOptionsComponent,
    MinesweeperGlobalOptionsComponent,
    TranslateModule
  ],
  entryComponents: [
    MinesweeperIngameOptionsComponent,
    MinesweeperGlobalOptionsComponent
  ],
  providers: [
    AuthenticationService,
    CookieService,
    { provide: MatPaginatorIntl, useValue: getPaginatorIntl() }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    library.add(
      faFlag,
      faBomb,
      faFireAlt,
      faShareAlt,
      faUsers,
      faUserLock,
      faUserAlt,
      faUserAltSlash,
      faInfoCircle,
      faCog,
      faFlag2,
      faSmile,
      faClock,
      faCopy,
      faSignInAlt,
      faLock,
      faLockOpen
    );
  }
}
