import { DifficultyEnum } from '../models/difficulty.enum';
import { EnumDictionary } from '../types/enum-dictionary';
import { Difficulty } from '../models/difficulty';

export const DIFFICULTY_SETS: EnumDictionary<DifficultyEnum, Difficulty> = {
    [DifficultyEnum.VERY_EASY]: {
        rows: 5,
        cols: 8,
        procentageOfMines: 0.1
    },
    [DifficultyEnum.EASY]: {
        rows: 7,
        cols: 10,
        procentageOfMines: 0.13
    },
    [DifficultyEnum.MEDIUM]: {
        rows: 10,
        cols: 12,
        procentageOfMines: 0.15
    },
    [DifficultyEnum.HARD]: {
        rows: 12,
        cols: 14,
        procentageOfMines: 0.17
    },
    [DifficultyEnum.VERY_HARD]: {
        rows: 16,
        cols: 16,
        procentageOfMines: 0.2
    }
};
