import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private translate: TranslateService,
    private cookieService: CookieService
  ) {
    this.translate.setDefaultLang('en');
    this.translate.use(this.cookieService.get('language') || 'en');
  }
}
