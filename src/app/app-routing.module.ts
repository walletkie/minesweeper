import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MinesweeperComponent } from './components/minesweeper/minesweeper.component';
import { TestComponent } from './components/test/test.component';
import { LoginComponent } from './components/login/login.component';
import { GenerateRoomComponent } from './components/generate-room/generate-room.component';
import { RoomsListComponent } from './components/rooms-list/rooms-list.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'rooms-list',
    component: RoomsListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'generate-room',
    component: GenerateRoomComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'room/:roomId',
    component: MinesweeperComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'test',
    component: TestComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
