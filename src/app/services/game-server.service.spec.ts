import { TestBed } from '@angular/core/testing';

import { GameServerService } from './game-server.service';

describe('GameServerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GameServerService = TestBed.get(GameServerService);
    expect(service).toBeTruthy();
  });
});
