import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Room } from '../models/room';
import { Game } from '../models/game';
import { AngularFireFunctions } from '@angular/fire/functions';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class GameServerService {

  private serverTimeDelta: number;

  constructor(
    public firestore: AngularFirestore,
    public functions: AngularFireFunctions,
    private authentication: AuthenticationService) {
    firestore.firestore.enablePersistence({ synchronizeTabs: false });
  }

  // Rooms

  getRoomsCollection(): AngularFirestoreCollection<Room> {
    return this.firestore.collection('rooms');
  }

  getRoom(roomId: string): AngularFirestoreDocument<Room> {
    return this.firestore.collection('rooms').doc(roomId);
  }

  createRoom(room: Room): Promise<firebase.firestore.DocumentReference> {
    return this.firestore.collection('rooms').add(room);
  }


  // Games

  createGame(roomId: string, game: Game): Promise<firebase.firestore.DocumentReference> {
    return this.firestore.collection(`rooms/${roomId}/games`).add(game);
  }

  getGame(roomId: string, gameId: string): AngularFirestoreDocument<Game> {
    return this.firestore.collection(`rooms/${roomId}/games`).doc(gameId);
  }


  // Functions

  async getServerTime(): Promise<number> {
    if (!this.serverTimeDelta) {
      const serverTimeCallable = this.functions.httpsCallable('getServerTime');
      const serverTime = await serverTimeCallable({}).toPromise();
      this.serverTimeDelta = Date.now() - serverTime;
    }
    return Date.now() - this.serverTimeDelta;
  }

  async getTimestampForRoom(): Promise<number> {
    const serverTime = await this.getServerTime();
    const serverTimeDate = new Date(serverTime);
    serverTimeDate.setSeconds(0);
    serverTimeDate.setMilliseconds(0);
    return serverTimeDate.getTime();
  }
}
