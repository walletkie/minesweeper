import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { BehaviorSubject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public isLoggedIn: BehaviorSubject<boolean>;
  public user: BehaviorSubject<User>;
  private serviceReady = false;

  constructor(
    public firestore: AngularFirestore,
    public fireAuth: AngularFireAuth) {
    this.isLoggedIn = new BehaviorSubject(false);
    this.user = new BehaviorSubject({} as User);
    fireAuth.auth.onAuthStateChanged(user => {
      this.assignUser(user, user ? true : false);
      this.serviceReady = true;
    });
  }

  async onLogout(): Promise<void> {
    if (this.fireAuth.auth.currentUser) {
      await this.fireAuth.auth.signOut();
    }
    this.assignUser({} as firebase.User, false);
  }

  assignUser(user: firebase.User, logged: boolean): void {
    if (logged) {
      this.isLoggedIn.next(true);
      if (user.isAnonymous) {
        this.user.next({
          uid: user.uid,
          name: 'Anonim',
          email: 'Bez adresu emial',
          photoUrl: 'https://ssl.gstatic.com/images/branding/product/1x/avatar_anonymous_square_512dp.png'
        });
      } else {
        this.user.next({
          uid: user.uid,
          name: user.displayName,
          email: user.email,
          photoUrl: user.photoURL
        });
      }
    } else {
      this.isLoggedIn.next(false);
      this.user.next({
        uid: null,
        name: null,
        email: null,
        photoUrl: null
      });
    }
  }

  async onLoginWithGoogle(): Promise<void> {
    const provider = new auth.GoogleAuthProvider();
    await this.fireAuth.auth.setPersistence(auth.Auth.Persistence.LOCAL);
    this.fireAuth.auth.signInWithPopup(provider).then((result) => this.assignUser(result.user, true))
      .catch((error) => {
        console.error(error);
      });
  }

  async onLoginWithFacebook(): Promise<void> {
    const provider = new auth.FacebookAuthProvider();
    await this.fireAuth.auth.setPersistence(auth.Auth.Persistence.LOCAL);
    this.fireAuth.auth.signInWithPopup(provider).then((result) => this.assignUser(result.user, true))
      .catch((error) => {
        console.error(error);
      });
  }

  async onLoginWithAnonumys(): Promise<void> {
    await this.fireAuth.auth.setPersistence(auth.Auth.Persistence.LOCAL);
    this.fireAuth.auth.signInAnonymously().then((result) => this.assignUser(result.user, true))
      .catch((error) => {
        console.error(error);
      });
  }

  timeout(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async authAwait(): Promise<boolean> {
    while (!this.serviceReady) {
      await this.timeout(100);
    }
    return true;
  }

  async getLoginStatus(): Promise<BehaviorSubject<boolean>> {
    await this.authAwait();
    return this.isLoggedIn;
  }
}
