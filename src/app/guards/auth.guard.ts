import { Injectable, NgZone } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authentication: AuthenticationService,
    private router: Router,
    private ngZone: NgZone) { }

  async canActivate(): Promise<boolean> {
    if (!(await this.authentication.getLoginStatus()).getValue()) {
      this.ngZone.run(() => this.router.navigateByUrl(`/`));
      return false;
    }
    return true;
  }

}
