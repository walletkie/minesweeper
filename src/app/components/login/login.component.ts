import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  private loginSubscription: Subscription;

  constructor(
    private authentication: AuthenticationService,
    private router: Router,
    private ngZone: NgZone) { }

  async ngOnInit() {
    this.loginSubscription = (await this.authentication.getLoginStatus()).subscribe((logged) => {
      if (logged) {
        this.ngZone.run(() => this.router.navigateByUrl(`/rooms-list`));
      }
    });
  }

  ngOnDestroy(): void {
    if (this.loginSubscription) {
      this.loginSubscription.unsubscribe();
    }
  }

  onLoginWithGoogle(): void {
    this.authentication.onLoginWithGoogle();
  }

  onLoginWithFacebook(): void {
    this.authentication.onLoginWithFacebook();
  }

  onLoginWithAnonumys(): void {
    this.authentication.onLoginWithAnonumys();
  }
}
