import { Component, OnInit, Input } from '@angular/core';
import { Room } from 'src/app/models/room';

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.scss']
})
export class PlayersListComponent implements OnInit {

  @Input()
  room: Room;

  constructor() { }

  ngOnInit() {
  }

}
