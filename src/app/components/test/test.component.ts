import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor(
  ) {
  }

  ngOnInit() {
  }

  test1() {
    console.log('test1');
  }

  async test2() {
    console.log('test2');
  }

}
