import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinesweeperGlobalOptionsComponent } from './minesweeper-global-options.component';

describe('MinesweeperGlobalOptionsComponent', () => {
  let component: MinesweeperGlobalOptionsComponent;
  let fixture: ComponentFixture<MinesweeperGlobalOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinesweeperGlobalOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinesweeperGlobalOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
