import { Component, OnInit, Inject, NgZone } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

// tslint:disable-next-line: no-empty-interface
export interface GlobalOptionsDialogData {
}

@Component({
  selector: 'app-minesweeper-global-options',
  templateUrl: './minesweeper-global-options.component.html',
  styleUrls: ['./minesweeper-global-options.component.scss']
})
export class MinesweeperGlobalOptionsComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<GlobalOptionsDialogData>,
    @Inject(MAT_DIALOG_DATA) public data: GlobalOptionsDialogData,
    public authentication: AuthenticationService,
    private ngZone: NgZone,
    public router: Router) {
  }

  ngOnInit() { }

  async onLogout(): Promise<void> {
    await this.authentication.onLogout();
    this.dialogRef.close();
    this.ngZone.run(() => this.router.navigateByUrl('/'));
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {
    this.dialogRef.close();
  }
}
