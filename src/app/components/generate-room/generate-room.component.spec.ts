import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateRoomComponent } from './generate-room.component';

describe('GenerateRoomComponent', () => {
  let component: GenerateRoomComponent;
  let fixture: ComponentFixture<GenerateRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
