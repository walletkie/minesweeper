import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Room } from 'src/app/models/room';
import { GameUtilities } from 'src/app/models/game';
import { DifficultyEnum } from 'src/app/models/difficulty.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { GameServerService } from 'src/app/services/game-server.service';

@Component({
  selector: 'app-generate-room',
  templateUrl: './generate-room.component.html',
  styleUrls: ['./generate-room.component.scss']
})
export class GenerateRoomComponent implements OnInit {

  constructor(
    private gameServer: GameServerService,
    private ngZone: NgZone,
    private router: Router) { }

  async ngOnInit() {
    const room: Room = {
      gameId: null,
      updateTimestamp: await this.gameServer.getTimestampForRoom(),
      players: {},
      locked: false
    };
    const roomDocumentRef = await this.gameServer.createRoom(room);

    const game = GameUtilities.createNewGame(DifficultyEnum.VERY_EASY);
    const gameDocumentRef = await this.gameServer.createGame(roomDocumentRef.id, game);

    const roomDocument = await this.gameServer.getRoom(roomDocumentRef.id);
    const a = await roomDocument.update({
      gameId: gameDocumentRef.id
    });

    this.ngZone.run(() => this.router.navigateByUrl(`/room/${roomDocumentRef.id}`, { replaceUrl: true }));
  }
}
