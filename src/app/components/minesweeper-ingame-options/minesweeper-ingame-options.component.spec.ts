import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinesweeperIngameOptionsComponent } from './minesweeper-ingame-options.component';

describe('MinesweeperIngameOptionsComponent', () => {
  let component: MinesweeperIngameOptionsComponent;
  let fixture: ComponentFixture<MinesweeperIngameOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinesweeperIngameOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinesweeperIngameOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
