import { Component, OnInit, Inject, NgZone } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { DifficultyEnum } from 'src/app/models/difficulty.enum';
import { DIFFICULTY_SETS } from 'src/app/values/difficulty-sets';

export interface IngameOptionsDialogData {
  difficulty: DifficultyEnum;
  locked: boolean;
}

@Component({
  selector: 'app-minesweeper-ingame-options',
  templateUrl: './minesweeper-ingame-options.component.html',
  styleUrls: ['./minesweeper-ingame-options.component.scss']
})
export class MinesweeperIngameOptionsComponent implements OnInit {
  public difficultyChoosen: string;
  public lockedChoosen: boolean;
  public DIFFICULTY_SETS = DIFFICULTY_SETS;
  public DifficultyEnum = DifficultyEnum;

  constructor(
    public dialogRef: MatDialogRef<MinesweeperIngameOptionsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IngameOptionsDialogData,
    public authentication: AuthenticationService,
    private ngZone: NgZone,
    public router: Router) {
    this.difficultyChoosen = String(data.difficulty);
    this.lockedChoosen = Boolean(data.locked);
  }

  ngOnInit() { }

  async onLogout(): Promise<void> {
    await this.authentication.onLogout();
    this.dialogRef.close();
    this.ngZone.run(() => this.router.navigateByUrl('/'));
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {
    const result: IngameOptionsDialogData = {
      ...this.data
    };

    if (this.difficultyChoosen !== String(this.data.difficulty)) {
      result.difficulty = Number(this.difficultyChoosen);
    }

    if (this.lockedChoosen !== Boolean(this.data.locked)) {
      result.locked = Boolean(this.lockedChoosen);
    }

    this.dialogRef.close(result);
  }

  byBoardSize(a: any, b: any) {
    return (a.value.cols * a.value.rows < b.value.cols * b.value.rows) ? 1 : -1;
  }
}
