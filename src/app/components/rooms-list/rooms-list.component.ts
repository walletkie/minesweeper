import { Component, OnInit, NgZone, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { Room, RoomElement, roomsAreEqual } from 'src/app/models/room';
import { Subscription } from 'rxjs';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { GameServerService } from 'src/app/services/game-server.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import {
  GlobalOptionsDialogData,
  MinesweeperGlobalOptionsComponent
} from '../minesweeper-global-options/minesweeper-global-options.component';

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.scss']
})
export class RoomsListComponent implements OnInit, OnDestroy {

  public joinToRoom = '';
  public roomsRef: AngularFirestoreCollection<Room>;

  public roomsListSubscription: Subscription;
  public roomsListDataLoaded = false;
  public roomsListDataSource = new MatTableDataSource<RoomElement>([]);
  public roomLastActiveTimeTimeout: any;

  @ViewChild('paginatorRef', { static: true }) paginatorRef: MatPaginator;
  public displayedColumns: string[] = ['locked', 'actions', 'roomId', 'lastTimeActive', 'players'];

  constructor(
    public gameServer: GameServerService,
    private router: Router,
    private ngZone: NgZone,
    public authentication: AuthenticationService,
    public dialog: MatDialog) {
  }

  async ngOnInit() {
    this.roomLastActiveTimeUpdater(0);
    this.roomsListDataSource.paginator = this.paginatorRef;
    this.roomsRef = this.gameServer.getRoomsCollection();
    this.roomsListSubscription = this.roomsRef.snapshotChanges().subscribe(async (v) => {
      const serverData = {};
      const serverTime = await this.gameServer.getServerTime();
      v.forEach((e) => {
        const data = {
          ...e.payload.doc.data(),
          roomId: e.payload.doc.id
        } as RoomElement;
        data.lastTimeActive = this.calcLastTimeActivelabel(data.updateTimestamp, serverTime);
        serverData[data.roomId] = data;
      });

      // Update list
      const currentRoomsList = this.roomsListDataSource.data;
      // tslint:disable-next-line: forin
      for (const key in serverData) {
        const mapRoom = serverData[key];
        const indexInTable = currentRoomsList.findIndex((e) => e.roomId === mapRoom.roomId);
        if (indexInTable !== -1) {
          if (!roomsAreEqual(mapRoom, currentRoomsList[indexInTable])) {
            currentRoomsList[indexInTable] = mapRoom;
          }
        } else {
          currentRoomsList.unshift(mapRoom);
        }
      }
      for (let i = 0; i < currentRoomsList.length; i++) {
        const listRoom = currentRoomsList[i];
        if (!serverData[listRoom.roomId]) {
          currentRoomsList.splice(i, 1);
          i--;
        }
      }
      this.roomsListDataLoaded = true;
      this.roomsListDataSource._updateChangeSubscription();
      console.log(this.roomsListDataSource.data);
    });
  }

  ngOnDestroy(): void {
    if (this.roomsListSubscription) {
      this.roomsListSubscription.unsubscribe();
    }
    if (this.roomLastActiveTimeTimeout) {
      clearTimeout(this.roomLastActiveTimeTimeout);
    }
  }

  calcLastTimeActivelabel(roomTimestamp: number, currentTimestamp: number): number {
    // less than 1 minute
    if (currentTimestamp - roomTimestamp < 60000) {
      return 0;
    }
    // less than 10 minutes
    if (currentTimestamp - roomTimestamp < 600000) {
      return 1;
    }
    // less than 5 hours
    if (currentTimestamp - roomTimestamp < 18000000) {
      return 2;
    }
    // more than 4 hours
    return 3;
  }

  roomLastActiveTimeUpdater(dealy: number): any {
    clearTimeout(this.roomLastActiveTimeTimeout);
    this.roomLastActiveTimeTimeout = setTimeout(
      async () => {
        const serverTime = await this.gameServer.getServerTime();
        this.roomsListDataSource.data.forEach((e) => {
          e.lastTimeActive = this.calcLastTimeActivelabel(e.updateTimestamp, serverTime);
        });
        this.roomLastActiveTimeUpdater(1000);
      },
      dealy
    );
  }

  onCreateRoom() {
    this.ngZone.run(() => this.router.navigateByUrl(`/generate-room`));
  }

  onJoinRoom(room?: RoomElement) {
    const roomToJoin = room ? room.roomId : this.joinToRoom;
    if (roomToJoin.length === 0) {
      return;
    }

    this.ngZone.run(() => this.router.navigateByUrl(`/room/${roomToJoin}`));
  }

  possibleToJoin(room: RoomElement) {
    return room.locked === false
      || this.authentication.user.getValue().uid in room.players;
  }

  onGlobalOptionsOpen(): void {
    const dialogData: GlobalOptionsDialogData = {};
    const dialogRef = this.dialog.open(MinesweeperGlobalOptionsComponent, {
      data: dialogData
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
    });
  }
}
