import { MatPaginatorIntl } from '@angular/material';

const rangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
        return `0 / ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;
    return `(${startIndex + 1} - ${endIndex}) / ${length}`;
};


export function getPaginatorIntl(): MatPaginatorIntl {
    const paginatorIntl = new MatPaginatorIntl();

    paginatorIntl.nextPageLabel = null;
    paginatorIntl.previousPageLabel = null;
    paginatorIntl.getRangeLabel = rangeLabel;
    paginatorIntl.firstPageLabel = null;
    paginatorIntl.lastPageLabel = null;

    return paginatorIntl;
}
