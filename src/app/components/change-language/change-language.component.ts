import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-change-language',
  templateUrl: './change-language.component.html',
  styleUrls: ['./change-language.component.scss']
})
export class ChangeLanguageComponent implements OnInit {

  public languages: Array<{
    code: string,
    name: string
  }> = [
      {
        code: 'pl',
        name: 'Polski'
      },
      {
        code: 'en',
        name: 'English'
      },
      {
        code: 'de',
        name: 'Deutsch'
      }
    ];

  public language: string;

  constructor(
    private cookieService: CookieService,
    private translate: TranslateService
  ) {
    this.language = cookieService.get('language') || 'en';
  }

  ngOnInit() {
  }

  onLanguageChange(value: string): void {
    this.cookieService.set('language', value);
    this.translate.use(value);
  }

}
