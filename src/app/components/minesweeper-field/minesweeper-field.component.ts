import { Component, Input, OnInit } from '@angular/core';
import { StatusEnum } from 'src/app/models/status.enum';
import { PlayerEnum } from 'src/app/models/player.enum';
import { Field } from 'src/app/models/field';

@Component({
  selector: 'app-minesweeper-field',
  templateUrl: './minesweeper-field.component.html',
  styleUrls: ['./minesweeper-field.component.scss']
})
export class MinesweeperFieldComponent implements OnInit {

  @Input()
  public field: Field;
  @Input()
  public gameStatus: StatusEnum;

  public value: string;
  public icon: Array<string>;
  public displayIcon: boolean;

  constructor() {
  }

  ngOnInit() {
    if (this.field.player === PlayerEnum.UNCHECKED) {
      this.displayIcon = false;
      this.value = '';
    } else if (this.field.player === PlayerEnum.MARKED_AS_MINE) {
      this.displayIcon = true;
      this.icon = ['fas', 'flag'];
    } else if (this.field.player === PlayerEnum.MARKED_AS_POSSIBLE_MINE) {
      this.displayIcon = false;
      this.value = '?';
    } else { // CHECKED
      if (this.field.mine) {
        this.displayIcon = true;
        this.icon = ['fas', 'bomb'];
      } else {
        this.displayIcon = false;
        if (this.field.value > 0) {
          this.value = this.field.value.toString();
        } else {
          this.value = '';
        }
      }
    }

    if (this.gameStatus === StatusEnum.WON || this.gameStatus === StatusEnum.LOST) {
      if (this.field.player === PlayerEnum.CHECKED) {
        if (this.field.mine) {
          this.displayIcon = true;
          this.icon = ['fas', 'fire-alt'];
        }
      } else if (this.field.player === PlayerEnum.MARKED_AS_MINE) {
        if (!this.field.mine) {
          this.displayIcon = true;
          this.icon = ['far', 'flag'];
        }
      } else { // UNCHECKED, MARKED_AS_POSSIBLE_MINE
        if (this.field.mine) {
          this.displayIcon = true;
          this.icon = ['fas', 'bomb'];
        } else {
          this.displayIcon = false;
          if (this.field.value > 0) {
            this.value = this.field.value.toString();
          } else {
            this.value = '';
          }
        }
      }
    }
  }

  isDisabled(): boolean {
    return this.field.player === PlayerEnum.CHECKED
      || this.gameStatus === StatusEnum.WON
      || this.gameStatus === StatusEnum.LOST;
  }

  isFocusDisabled(): boolean {
    return this.field.value === 0;
  }

}
