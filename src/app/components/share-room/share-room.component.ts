import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-share-room',
  templateUrl: './share-room.component.html',
  styleUrls: ['./share-room.component.scss']
})
export class ShareRoomComponent implements OnInit {

  @Input()
  roomId: string;

  shareInputRoom: string;

  @ViewChild('inputRoomRef', { static: true })
  inputRoomRef: ElementRef;

  @ViewChild('shareInputRoomRef', { static: true })
  shareInputRoomRef: ElementRef;

  constructor(
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.shareInputRoom = window.location.href;
  }

  async onCopyRoom(): Promise<void> {
    const ref = this.inputRoomRef.nativeElement;
    ref.select();
    ref.setSelectionRange(0, 99999);
    document.execCommand('copy');
    this.snackBar.open(
      await this.translate.get('share-room.copied-name').toPromise(),
      await this.translate.get('common.close').toPromise(),
      { duration: 1500 }
    );
  }

  async onShareRoom(): Promise<void> {
    const ref = this.shareInputRoomRef.nativeElement;
    ref.select();
    ref.setSelectionRange(0, 99999);
    document.execCommand('copy');
    this.snackBar.open(
      await this.translate.get('share-room.copied-link').toPromise(),
      await this.translate.get('common.close').toPromise(),
      { duration: 1500 }
    );
  }

}
