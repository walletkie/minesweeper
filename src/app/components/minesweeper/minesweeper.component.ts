import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Room } from 'src/app/models/room';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { Game, GameUtilities } from 'src/app/models/game';
import { DifficultyEnum } from 'src/app/models/difficulty.enum';
import { StatusEnum } from 'src/app/models/status.enum';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import {
  MinesweeperIngameOptionsComponent,
  IngameOptionsDialogData
} from '../minesweeper-ingame-options/minesweeper-ingame-options.component';
import { Player } from 'src/app/models/player';
import { GameServerService } from 'src/app/services/game-server.service';

@Component({
  selector: 'app-minesweeper',
  templateUrl: './minesweeper.component.html',
  styleUrls: ['./minesweeper.component.scss']
})
export class MinesweeperComponent implements OnInit, OnDestroy {
  public roomId: string;
  public room: Room;
  public roomRef: AngularFirestoreDocument<Room>;
  public roomRefSubscription: Subscription;

  public game: Game;
  public gameRef: AngularFirestoreDocument<Game>;
  public gameRefSubscription: Subscription;

  public timerValue: string;
  public timer: any;

  public error: boolean;

  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private authentication: AuthenticationService,
    private gameServer: GameServerService) { }


  async ngOnInit() {
    this.roomId = this.route.snapshot.paramMap.get('roomId');
    if (this.roomId) {
      this.roomRef = this.gameServer.getRoom(this.roomId);
      this.roomRefSubscription = this.roomRef.valueChanges().subscribe((roomValue) => {
        if (roomValue) {
          if (!this.room) {
            // Called once, within first game load
            this.room = roomValue;
            this.gameRef = this.gameServer.getGame(this.roomId, this.room.gameId);
            this.gameRefSubscription = this.gameRef.valueChanges().subscribe(
              (gameValue) => {
                if (!this.game
                  || this.game.status !== gameValue.status
                  || this.game.difficulty !== gameValue.difficulty) {
                  this.game = gameValue;
                } else {
                  GameUtilities.mergeGameBoard(this.game, gameValue);
                }
              },
              () => {
                // Cannot get game
                this.error = true;
              });

            const uid = this.authentication.user.getValue().uid;
            const player: Player = {
              name: this.authentication.user.getValue().name,
              photoUrl: this.authentication.user.getValue().photoUrl
            };
            if (!(uid in this.room.players)
              || player.name !== this.room.players[uid].name
              || player.photoUrl !== this.room.players[uid].photoUrl) {
              const palyerUpdate: { [uid: string]: Player } = {};
              palyerUpdate[`players.${uid}`] = player;
              this.roomRef.update(palyerUpdate);
            }

          } else {
            this.room.updateTimestamp = roomValue.updateTimestamp;
            this.room.locked = roomValue.locked;
            for (const key in roomValue.players) {
              if (!this.room.players[key]) {
                this.room.players[key] = roomValue.players[key];
              }
            }
          }
        } else {
          // Room not found
          this.error = true;
        }

        console.log(roomValue);
      });
      this.timerLoop(0);
    }
  }

  ngOnDestroy() {
    if (this.roomRefSubscription) {
      this.roomRefSubscription.unsubscribe();
    }
    if (this.gameRefSubscription) {
      this.gameRefSubscription.unsubscribe();
    }
    clearTimeout(this.timer);
  }

  timerLoop(dealy: number): void {
    clearTimeout(this.timer);
    this.timer = setTimeout(async () => {
      if (this.room && this.game && this.game.timer.startDate) {
        let displayedValue = 0;
        if (this.game.timer.endDate) {
          displayedValue = this.game.timer.endDate - this.game.timer.startDate;
        } else {
          displayedValue = await this.gameServer.getServerTime() - this.game.timer.startDate;
        }
        displayedValue = Math.floor(displayedValue / 1000);
        if (displayedValue > 999) {
          this.timerValue = '999+';
        } else {
          this.timerValue = String(displayedValue);
        }
      } else {
        this.timerValue = '0';
      }
      this.timerLoop(200);
    }, dealy);
  }

  isGameWon() {
    return this.game.status === StatusEnum.WON;
  }

  isGameLost() {
    return this.game.status === StatusEnum.LOST;
  }

  countMinesLeftToMark(): number {
    return GameUtilities.countMinesLeftToMark(this.game);
  }

  async onUpdateGame(): Promise<void> {
    const newTimestamp = await this.gameServer.getTimestampForRoom();
    const currentValue = this.room.updateTimestamp;
    if (newTimestamp !== currentValue) {
      this.roomRef.update({
        updateTimestamp: newTimestamp
      });
    }
  }

  onGameReset(difficulty?: DifficultyEnum): void {
    this.gameRef.set(GameUtilities.createNewGame(
      difficulty !== undefined ? difficulty : this.game.difficulty
    ));
    this.onUpdateGame();
  }

  onRoomLockChange(lock: boolean): void {
    this.roomRef.update({ locked: lock });
    this.onUpdateGame();
  }

  onOpenOptions(): void {
    const dialogData: IngameOptionsDialogData = {
      difficulty: this.game.difficulty,
      locked: this.room.locked
    };
    const dialogRef = this.dialog.open(MinesweeperIngameOptionsComponent, {
      data: dialogData
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      const newOptions = result as IngameOptionsDialogData;

      if (newOptions.difficulty !== this.game.difficulty) {
        this.onGameReset(newOptions.difficulty);
      }

      if (newOptions.locked !== this.room.locked) {
        this.onRoomLockChange(newOptions.locked);
      }
    });
  }
}
