import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Game, GameUtilities } from 'src/app/models/game';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { PlayerEnum } from 'src/app/models/player.enum';
import { Difficulty } from 'src/app/models/difficulty';
import { StatusEnum } from 'src/app/models/status.enum';
import { Field } from 'src/app/models/field';
import { GameServerService } from 'src/app/services/game-server.service';

@Component({
  selector: 'app-minesweeper-board',
  templateUrl: './minesweeper-board.component.html',
  styleUrls: ['./minesweeper-board.component.scss']
})
export class MinesweeperBoardComponent implements OnInit, OnChanges {

  @Input()
  game: Game;

  @Input()
  gameRef: AngularFirestoreDocument<Game>;

  // tslint:disable-next-line: no-output-on-prefix
  @Output() onUpdateGame: EventEmitter<any> = new EventEmitter();

  difficulty: Difficulty;

  constructor(
    public gameServer: GameServerService) { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.difficulty = GameUtilities.getDifficulty(this.game);
  }

  indexWithinLimits(value: number, min: number, max: number): boolean {
    return value >= min && value <= max;
  }

  allMinesArroudAreMarked(row: number, col: number): boolean {
    return true;
  }

  getClickedFields(row: number, col: number, forceCheck: boolean = false, clickedMap = {}): any {
    const currentField = this.game.board[row][col];
    const id = `board.${row}.${col}.player`;
    clickedMap[id] = PlayerEnum.CHECKED;

    if ((currentField.value === 0 && currentField.player === PlayerEnum.UNCHECKED) || (forceCheck)) {
      this.forEachFieldArround(row, col, (field, i, j) => {
        if (!(`board.${i}.${j}.player` in clickedMap)
          && field.player !== PlayerEnum.MARKED_AS_MINE) {
          this.getClickedFields(i, j, false, clickedMap);
        }
      });
    }

    return clickedMap;
  }

  isGameLost(): boolean {
    for (let i = 0; i < this.difficulty.rows; i++) {
      for (let j = 0; j < this.difficulty.cols; j++) {
        const field = this.game.board[i][j];
        if (field.player === PlayerEnum.CHECKED && field.mine === true) {
          return true;
        }
      }
    }
    return false;
  }

  isGameWon(): boolean {
    let unCheckedFields = 0;
    for (let i = 0; i < this.difficulty.rows; i++) {
      for (let j = 0; j < this.difficulty.cols; j++) {
        const field = this.game.board[i][j];
        // count unchecked and marked as mine
        if (field.player !== PlayerEnum.CHECKED) {
          unCheckedFields++;
        }
      }
    }
    return unCheckedFields === GameUtilities.getTotalMinesAmmount(this.game);
  }

  forEachFieldArround(row: number, col: number, handler: (field: Field, row: number, col: number) => void): void {
    for (let i = row - 1; i <= row + 1; i++) {
      if (this.indexWithinLimits(i, 0, this.difficulty.rows - 1)) {
        for (let j = col - 1; j <= col + 1; j++) {
          if (this.indexWithinLimits(j, 0, this.difficulty.cols - 1)) {
            const field = this.game.board[i][j];
            handler(field, i, j);
          }
        }
      }
    }
  }

  countMinesArround(row: number, col: number): number {
    let countedBoardValue = 0;
    this.forEachFieldArround(row, col, (field) => {
      if (field.mine === true) {
        countedBoardValue++;
      }
    });
    return countedBoardValue;
  }

  countMarkedAsMinesArround(row: number, col: number): number {
    let countedMarkedMinesValue = 0;
    this.forEachFieldArround(row, col, (field) => {
      if (field.player === PlayerEnum.MARKED_AS_MINE) {
        countedMarkedMinesValue++;
      }
    });
    return countedMarkedMinesValue;
  }

  getRandomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  possibleToPlace(
    field: Field,
    notInRow: number,
    notInCol: number,
    row: number,
    col: number): boolean {
    if (field.mine === true) {
      return false;
    }
    if ((row >= notInRow - 2 && row <= notInRow + 2) && (col >= notInCol - 2 && col <= notInCol + 2)) {
      return false;
    }
    return true;
  }

  insertMineAtRandomPlace(notInRow: number, notInCol: number): void {
    const row = this.getRandomInt(0, this.difficulty.rows - 1);
    const col = this.getRandomInt(0, this.difficulty.cols - 1);
    const field = this.game.board[row][col];

    if (this.possibleToPlace(field, notInRow, notInCol, row, col)) {
      field.mine = true;
    } else {
      // try again
      this.insertMineAtRandomPlace(notInRow, notInCol);
    }
  }

  async startNewGame(startRow: number, startCol: number): Promise<void> {
    const totalMinesAmmount = GameUtilities.getTotalMinesAmmount(this.game);

    // generate mines
    for (let i = 0; i < totalMinesAmmount; i++) {
      this.insertMineAtRandomPlace(startRow, startCol);
    }
    // assing values
    for (let i = 0; i < this.difficulty.rows; i++) {
      for (let j = 0; j < this.difficulty.cols; j++) {
        const field = this.game.board[i][j];
        field.value = this.countMinesArround(i, j);
      }
    }
    this.game.status = StatusEnum.IN_GAME;
    this.game.timer.startDate = await this.gameServer.getServerTime();
  }

  async fieldClicked(row: any, col: any): Promise<void> {
    row = Number(row);
    col = Number(col);
    console.log(this.game.board[row][col]);

    // not possible to click
    if (this.game.board[row][col].player === PlayerEnum.MARKED_AS_MINE
      || this.game.board[row][col].player === PlayerEnum.MARKED_AS_POSSIBLE_MINE) {
      return;
    }

    // game already ended
    if (this.game.status === StatusEnum.LOST
      || this.game.status === StatusEnum.WON) {
      return;
    }

    // check if game had been generated, if not then generate it
    if (this.game.status === StatusEnum.NOT_GENERATED) {
      await this.startNewGame(row, col);
      await this.gameRef.set(this.game);
    }

    let forceCheckArround = false;
    if (this.game.board[row][col].player === PlayerEnum.CHECKED) {
      forceCheckArround = this.countMarkedAsMinesArround(row, col) >= this.game.board[row][col].value;
    }
    console.log(forceCheckArround);
    const clickedFields = this.getClickedFields(row, col, forceCheckArround);
    await this.gameRef.update(clickedFields);
    console.log(clickedFields);

    if (this.isGameLost()) {
      const gameStatus = {
        status: StatusEnum.LOST
      };
      gameStatus['timer.endDate'] = await this.gameServer.getServerTime();
      await this.gameRef.update(gameStatus);
    } else if (this.isGameWon()) {
      const gameStatus = {
        status: StatusEnum.WON
      };
      gameStatus['timer.endDate'] = await this.gameServer.getServerTime();
      await this.gameRef.update(gameStatus);
    }

    this.onUpdateGame.emit();
  }

  async fieldLongClicked(row: any, col: any): Promise<void> {
    row = Number(row);
    col = Number(col);

    // possible to click
    if (this.game.board[row][col].player === PlayerEnum.CHECKED
      || this.game.status === StatusEnum.NOT_GENERATED) {
      return;
    }

    // game already ended
    if (this.game.status === StatusEnum.LOST
      || this.game.status === StatusEnum.WON) {
      return;
    }

    const field = this.game.board[row][col];
    let nextValue: PlayerEnum;
    switch (field.player) {
      case PlayerEnum.UNCHECKED:
        nextValue = PlayerEnum.MARKED_AS_MINE;
        break;

      case PlayerEnum.MARKED_AS_MINE:
        nextValue = PlayerEnum.MARKED_AS_POSSIBLE_MINE;
        break;

      case PlayerEnum.MARKED_AS_POSSIBLE_MINE:
        nextValue = PlayerEnum.UNCHECKED;
        break;

      default:
        break;
    }

    // no more marks left
    if (GameUtilities.countMinesLeftToMark(this.game) <= 0 && nextValue === PlayerEnum.MARKED_AS_MINE) {
      return;
    }

    const updateObject = {};
    updateObject[`board.${row}.${col}.player`] = nextValue;
    // Vibration for mobiles
    if (window.navigator) {
      window.navigator.vibrate(200);
    }
    await this.gameRef.update(updateObject);
    this.onUpdateGame.emit();
  }

  keyOrder(a: any, b: any): number {
    return Number(a.key) > Number(b.key) ? 1 : -1;
  }
}
