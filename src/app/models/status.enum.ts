export enum StatusEnum {
    NOT_GENERATED,
    IN_GAME,
    WON,
    LOST
}
