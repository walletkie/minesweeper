export enum PlayerEnum {
    UNCHECKED,
    CHECKED,
    MARKED_AS_MINE,
    MARKED_AS_POSSIBLE_MINE
}
