import { Field } from './field';
import { DifficultyEnum } from './difficulty.enum';
import { StatusEnum } from './status.enum';
import { DIFFICULTY_SETS } from '../values/difficulty-sets';
import { PlayerEnum } from './player.enum';
import { Difficulty } from './difficulty';

export interface Game {
    board: {
        [row: number]: {
            [col: number]: Field
        }
    };
    difficulty: DifficultyEnum;
    status: StatusEnum;
    timer: {
        startDate: number,
        endDate: number
    };
}

export class GameUtilities {
    public static createNewGame(difficulty: DifficultyEnum): Game {
        const game: Game = {
            board: {},
            difficulty,
            status: StatusEnum.NOT_GENERATED,
            timer: {
                startDate: null,
                endDate: null
            }
        };

        const difficultyObject = DIFFICULTY_SETS[difficulty];
        for (let i = 0; i < difficultyObject.rows; i++) {
            game.board[i] = {};
            for (let j = 0; j < difficultyObject.cols; j++) {
                game.board[i][j] = {
                    mine: false,
                    player: PlayerEnum.UNCHECKED,
                    value: 0
                };
            }
        }

        return game;
    }

    public static getDifficulty(game: Game): Difficulty {
        return DIFFICULTY_SETS[game.difficulty];
    }

    public static getTotalMinesAmmount(game: Game): number {
        const difficulty = GameUtilities.getDifficulty(game);
        const minesCount = difficulty.rows * difficulty.cols * difficulty.procentageOfMines;
        return Math.floor(minesCount);
    }

    public static getTotalMarkedAsMineAmmount(game: Game): number {
        let markedAsMine = 0;
        const difficulty = GameUtilities.getDifficulty(game);
        for (let i = 0; i < difficulty.rows; i++) {
            for (let j = 0; j < difficulty.cols; j++) {
                if (game.board[i][j].player === PlayerEnum.MARKED_AS_MINE) {
                    markedAsMine++;
                }
            }
        }
        return markedAsMine;
    }

    public static countMinesLeftToMark(game: Game): number {
        return GameUtilities.getTotalMinesAmmount(game) - GameUtilities.getTotalMarkedAsMineAmmount(game);
    }

    public static mergeGameBoard(base: Game, newChanges: Game): void {
        const difficulty = GameUtilities.getDifficulty(base);
        for (let row = 0; row < difficulty.rows; row++) {
            for (let col = 0; col < difficulty.cols; col++) {
                if (base.board[row][col].player !== newChanges.board[row][col].player) {
                    base.board[row][col] = newChanges.board[row][col];
                }
            }
        }
    }
}
