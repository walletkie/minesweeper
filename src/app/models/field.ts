import { PlayerEnum } from './player.enum';

export interface Field {
    mine: boolean;
    value: number;
    player: PlayerEnum;
}
