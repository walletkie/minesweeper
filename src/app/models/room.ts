import { Player } from './player';

export interface Room {
    gameId: string;
    updateTimestamp: number;
    locked: boolean;
    players: {
        [uId: string]: Player
    };
}

export interface RoomElement extends Room {
    roomId: string;
    lastTimeActive: number;
}

export interface RoomsMap {
    [roomId: string]: RoomElement;
}

export function roomsAreEqual(room1: RoomElement, room2: RoomElement) {
    if (room1.gameId !== room2.gameId) {
        return false;
    }
    if (room1.locked !== room2.locked) {
        return false;
    }
    if (room1.roomId !== room2.roomId) {
        return false;
    }
    if (room1.updateTimestamp !== room2.updateTimestamp) {
        return false;
    }
    if (Object.keys(room1.players).length !== Object.keys(room2.players).length) {
        return false;
    }
    // tslint:disable-next-line: forin
    for (const key in room1.players) {
        const player1 = room1.players[key];
        const player2 = room2.players[key];
        if (!player1 || !player2) {
            return false;
        }
        if (player1.name !== player2.name) {
            return false;
        }
        if (player1.photoUrl !== player2.photoUrl) {
            return false;
        }
    }
    return true;
}
