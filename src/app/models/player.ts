export interface Player {
    name: string;
    photoUrl: string;
}
