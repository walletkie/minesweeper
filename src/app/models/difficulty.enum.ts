export enum DifficultyEnum {
    VERY_EASY,
    EASY,
    MEDIUM,
    HARD,
    VERY_HARD
}
