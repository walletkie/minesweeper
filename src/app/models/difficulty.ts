export interface Difficulty {
    rows: number;
    cols: number;
    procentageOfMines: number;
}
