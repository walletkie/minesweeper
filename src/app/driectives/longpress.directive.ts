import {
  Directive,
  Input,
  Output,
  EventEmitter,
  HostListener
} from '@angular/core';

@Directive({
  selector: '[appLongpress]'
})
export class LongpressDirective {
  @Input() duration = 500;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onPress: EventEmitter<any> = new EventEmitter();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onLongPress: EventEmitter<any> = new EventEmitter();

  private primaryButton: boolean;
  private pressing: boolean;
  private longPressed: boolean;
  private canceled: boolean;
  private timeout: any;
  private startMouseX: number;
  private startMouseY: number;

  @HostListener('mousedown', ['$event'])
  onMouseDown(event: MouseEvent) {
    event.preventDefault();

    this.primaryButton = event.buttons === 1;
    if (!this.primaryButton) {
      return;
    }

    this.startMouseX = event.clientX;
    this.startMouseY = event.clientY;
    this.pressing = true;
    this.longPressed = false;
    this.canceled = false;

    this.timeout = setTimeout(() => {
      this.longPressed = true;
      this.endPress();
    }, this.duration);
  }

  @HostListener('contextmenu', ['$event'])
  onContextMenu(event: MouseEvent) {
    event.preventDefault();

    this.pressing = true;
    this.canceled = false;
    this.longPressed = true;
    this.endPress();
  }

  @HostListener('mouseup')
  onMouseUp() {
    if (!this.primaryButton) {
      return;
    }

    this.endPress();
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    if (this.pressing) {
      this.canceled = true;
      this.endPress();
    }
  }

  @HostListener('mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    if (this.pressing) {
      const xThres = Math.abs(event.clientX - this.startMouseX) > 10;
      const yThres = Math.abs(event.clientY - this.startMouseY) > 10;
      if (xThres || yThres) {
        this.canceled = true;
        this.endPress();
      }
    }
  }

  endPress() {
    clearTimeout(this.timeout);

    if (this.canceled || !this.pressing) {
      return;
    }

    this.pressing = false;
    if (this.longPressed || !this.primaryButton) {
      this.onLongPress.emit();
    } else {
      this.onPress.emit();
    }
  }
}
